package com.paynova.api.client.model;

import java.math.BigDecimal;


public class Component extends ApiPropertyContainer {

   private String id;

   private String articleNumber;

   private String name;

   private String description;

   private String productUrl;

   private float quantity;

   private String unitMeasure;

   private BigDecimal unitAmountExcludingTax;

   private float taxPercent;

   private BigDecimal totalComponentTaxAmount;

   private BigDecimal totalComponentAmount;



   /**
    * Class constructor.
    * @param id	the id for this component item. This value must be unique per collection of component items.
    * @param articleNumber		the article/product number for the item being sold. Validation: Minimum
    * length = 1, Maximum length = 50
    * @param name	the name of the item being sold. Validation: Minimum length = 1, Maximum length = 255
    * @param quantity	the number of items being sold at this price.
    * @param unitMeasure	the unit of measure of the product/service being sold. Example Values: meters,
    * pieces, st., ea.
    * @param unitAmountExcludingTax	the price of each unit, excluding tax.
    * @param taxPercent	the tax/VAT percentage for the item being sold.
    * @param totalComponentTaxAmount	the total tax/VAT amount charged for this component.
    */
   public Component(String id, String articleNumber, String name, float quantity,
                   String unitMeasure, BigDecimal unitAmountExcludingTax, float taxPercent,
                   BigDecimal totalComponentTaxAmount, BigDecimal totalComponentAmount) {
      setId(id);
      setArticleNumber(articleNumber);
      setName(name);
      setQuantity(quantity);
      setUnitMeasure(unitMeasure);
      setUnitAmountExcludingTax(unitAmountExcludingTax);
      setTaxPercent(taxPercent);
      setTotalComponentTaxAmount(totalComponentTaxAmount);
      setTotalComponentAmount(totalComponentAmount);
   }

   /**
    *
    * @return the id for this Component
    */
   public String getId() {
      return id;
   }

   /**
    * The id for this component item. This value must be unique per collection of component items.
    * @param id
    * @return the current Component object
    */
   public Component setId(String id) {
      this.id = id;
      return this;
   }

   /**
    *
    * @return the article article number of this Component
    */
   public String getArticleNumber() {
      return articleNumber;
   }

   /**
    * The article/product number for the item being sold. Validation: Minimum length = 1,
    * Maximum length = 50
    * @param articleNumber
    * @return the current Component object
    */
   public Component setArticleNumber(String articleNumber) {
      this.articleNumber = articleNumber;
      return this;
   }

   /**
    *
    * @return the name of this Component
    */
   public String getName() {
      return name;
   }

   /**
    * The name of the item being sold. Validation: Minimum length = 1, Maximum length = 255
    * @param name
    * @return the current Component object
    */
   public Component setName(String name) {
      this.name = name;
      return this;
   }

   /**
    *
    * @return the description of this Component
    */
   public String getDescription() {
      return description;
   }

   /**
    * The description of the item being sold. Validation: Minimum length (when present) = 1,
    * Maximum length = 255
    * @param description
    * @return the current Component object
    */
   public Component setDescription(String description) {
      this.description = description;
      return this;
   }

   /**
    *
    * @return the url to this Component
    */
   public String getProductUrl() {
      return productUrl;
   }

   /**
    * The URL on your website to the item being sold.
    * @param productUrl
    * @return the current Component object
    */
   public Component setProductUrl(String productUrl) {
      this.productUrl = productUrl;
      return this;
   }

   /**
    *
    * @return the quantity of this Component
    */
   public float getQuantity() {
      return quantity;
   }

   /**
    * The number of items being sold at this price.
    * @param quantity
    * @return the current Component object
    */
   public Component setQuantity(float quantity) {
      this.quantity = quantity;
      return this;
   }

   /**
    *
    * @return the unit measure of this Component
    */
   public String getUnitMeasure() {
      return unitMeasure;
   }

   /**
    * The unit of measure of the product/service being sold. Example Values: meters,
    * pieces, st., ea.
    * @param unitMeasure
    * @return the current Component object
    */
   public Component setUnitMeasure(String unitMeasure) {
      this.unitMeasure = unitMeasure;
      return this;
   }

   /**
    *
    * @return the price per unit of this Component
    */
   public BigDecimal getUnitAmountExcludingTax() {
      return unitAmountExcludingTax;
   }

   /**
    * The price of each unit, excluding tax.
    * @param unitAmountExcludingTax
    * @return the current Component object
    */
   public Component setUnitAmountExcludingTax(BigDecimal unitAmountExcludingTax) {
      this.unitAmountExcludingTax = unitAmountExcludingTax;
      return this;
   }

   /**
    *
    * @return the tax/VAT percent for this Component
    */
   public float getTaxPercent() {
      return taxPercent;
   }

   /**
    * The tax/VAT percentage for the item being sold.
    * @param taxPercent
    * @return the current Component object
    */
   public Component setTaxPercent(float taxPercent) {
      this.taxPercent = taxPercent;
      return this;
   }

   /**
    *
    * @return the total tax/VAT amount
    */
   public BigDecimal getTotalComponentTaxAmount() {
      return totalComponentTaxAmount;
   }

   /**
    * The total tax/VAT amount charged for this component.
    * @param totalComponentTaxAmount
    * @return the current Component object
    */
   public Component setTotalComponentTaxAmount(BigDecimal totalComponentTaxAmount) {
      this.totalComponentTaxAmount = totalComponentTaxAmount;
      return this;
   }


   public BigDecimal getTotalComponentAmount() {
      return totalComponentAmount;
   }

   public Component setTotalComponentAmount(BigDecimal totalComponentAmount) {
      this.totalComponentAmount = totalComponentAmount;
      return this;
   }

}
