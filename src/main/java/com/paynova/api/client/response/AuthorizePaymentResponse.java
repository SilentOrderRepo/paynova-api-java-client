package com.paynova.api.client.response;

public class AuthorizePaymentResponse extends Response
{
   private String orderId;

   private String transactionId;

   private String acquirerId;

   private String acquirerReferenceId;

   public String getOrderId()
   {
      return orderId;
   }

   public void setOrderId( String orderId )
   {
      this.orderId = orderId;
   }

   public String getTransactionId()
   {
      return transactionId;
   }

   public void setTransactionId( String transactionId )
   {
      this.transactionId = transactionId;
   }

   public String getAcquirerId()
   {
      return acquirerId;
   }

   public void setAcquirerId( String acquirerId )
   {
      this.acquirerId = acquirerId;
   }

   public String getAcquirerReferenceId()
   {
      return acquirerReferenceId;
   }

   public void setAcquirerReferenceId( String acquirerReferenceId )
   {
      this.acquirerReferenceId = acquirerReferenceId;
   }
}
