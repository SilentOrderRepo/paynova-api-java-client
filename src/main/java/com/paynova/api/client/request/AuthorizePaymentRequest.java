package com.paynova.api.client.request;

import com.paynova.api.client.model.ProfilePaymentOptions;

import java.math.BigDecimal;

public class AuthorizePaymentRequest extends Request
{

   public static final String InvoicePayment = "InvoicePayment";
   public static final String RecurringPayment = "RecurringPayment";
   public static final String ProfilePayment = "ProfilePayment";

   private BigDecimal totalAmount;

   private String transactionId;

   private Integer paymentChannelId;

   private String authorizationType;

   private Integer paymentMethodId;

   private String paymentMethodProductId;

   private ProfilePaymentOptions profilePaymentOptions;

   private String orderId;
   private String routingIndicator;

   public AuthorizePaymentRequest( String orderId, BigDecimal totalAmount, String authorizationType )
   {
      this.orderId = orderId;
      this.totalAmount = totalAmount;
      this.authorizationType = authorizationType;
   }

   @Override
   public String getRestPath()
   {
      return "orders/" + getOrderId() + "/authorizePayment";
   }

   String getOrderId()
   {
      return orderId;
   }

   public void setOrderId( String orderId )
   {
      this.orderId = orderId;
   }

   public BigDecimal getTotalAmount()
   {
      return totalAmount;
   }

   public void setTotalAmount( BigDecimal totalAmount )
   {
      this.totalAmount = totalAmount;
   }

   public String getTransactionId()
   {
      return transactionId;
   }

   public void setTransactionId( String transactionId )
   {
      this.transactionId = transactionId;
   }

   public Integer getPaymentChannelId()
   {
      return paymentChannelId;
   }

   public void setPaymentChannelId( Integer paymentChannelId )
   {
      this.paymentChannelId = paymentChannelId;
   }

   public String getAuthorizationType()
   {
      return authorizationType;
   }

   public void setAuthorizationType( String authorizationType )
   {
      this.authorizationType = authorizationType;
   }

   public Integer getPaymentMethodId()
   {
      return paymentMethodId;
   }

   public void setPaymentMethodId( Integer paymentMethodId )
   {
      this.paymentMethodId = paymentMethodId;
   }

   public String getPaymentMethodProductId()
   {
      return paymentMethodProductId;
   }

   public void setPaymentMethodProductId( String paymentMethodProductId )
   {
      this.paymentMethodProductId = paymentMethodProductId;
   }

   public ProfilePaymentOptions getProfilePaymentOptions()
   {
      return profilePaymentOptions;
   }

   public void setProfilePaymentOptions( ProfilePaymentOptions profilePaymentOptions )
   {
      this.profilePaymentOptions = profilePaymentOptions;
   }


   public void setRoutingIndicator( String routingIndicator )
   {
      this.routingIndicator = routingIndicator;
   }

   public String getRoutingIndicator()
   {
      return routingIndicator;
   }
}
